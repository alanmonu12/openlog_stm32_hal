/*
 * OpenLog.c
 *
 *  Created on: 21/10/2018
 *      Author: Pavilion DM1
 */

#include <string.h>
#include <stdio.h>

#include "OpenLog.h"

#include "main.h"
#include "usart.h"
#include "gpio.h"

#include "stm32f1xx_hal.h"

extern UART_HandleTypeDef huart1;
uint8_t command_mode_cmd[3] = {CTRLZ,CTRLZ,CTRLZ};
const char NewFile_cmd[] = "new ";
const char Append_cmd[] = "append ";
const char init_cmd[] = "12<";
const char command_mode[] = "12>";
const uint8_t CR = 13;

static char buffer[3];

#define OPENLOG_UART_HANDLE	huart1

OpenLog_status OpenLog_init(void)
{
	//Poniendo en estado bajo el pin de reset se reinicia el OpenLog
	//para poder usarlo cuando esta en modo NewLog mode
	HAL_GPIO_WritePin(IO_SIRENA_GPIO_Port, IO_SIRENA_Pin, GPIO_PIN_RESET);
	HAL_Delay(10);
	HAL_GPIO_WritePin(IO_SIRENA_GPIO_Port, IO_SIRENA_Pin, GPIO_PIN_SET);

	/*Debemos esperar a que el modulo nos responda 12< para saber que esta listo
	* el 1 significa que la comunicacion serial esta lista el 2 significa que
	* la memoria SD esta lsita y el < significa que el modulo esta listo
	* para recibir caracteres por el puerto serial
	*/
	HAL_UART_Receive_IT(&OPENLOG_UART_HANDLE,(uint8_t*)buffer,sizeof(buffer));

	if(memcmp(buffer,init_cmd,sizeof(buffer)) != 0)
	{
		return OpenLog_ERROR;
	}
	else
	{
		return OpenLog_OK;
	}
}



OpenLog_status OpenLog_CommandMode(void)
{
	static char buffer[3];

	/*
	 * Se envian los tres caracteres para hacer que el OpenLong entre en
	 * command mode
	 */
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,&command_mode_cmd[0],3,100);
	/*Debemos esperar a que el modulo nos responda 12> para saber que esta listo
	* el 1 significa que la comunicacion serial esta lista el 2 significa que
	* la memoria SD esta lsita y el > significa que el modulo esta en command
	* mode
	*/
	HAL_UART_Receive(&OPENLOG_UART_HANDLE,(uint8_t*)buffer,sizeof(buffer),100);
	if(memcmp(buffer,command_mode,sizeof(buffer)) != 0)
	{
		return OpenLog_ERROR;
	}
	else
	{
		return OpenLog_OK;
	}
}

OpenLog_status OpenLog_NewFile(char* FileName)
{
	static char buffer[3];
	OpenLog_status Status;

	/*
	 * Se envia "new " para decirle al modulo que va a crear un nuevo archivo
	 * seguido del combre del archivo "FileName.txt" y por ultimo un retorno
	 * de carro para que el modulo sepa que tiene que crear el archivo
	 */
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)NewFile_cmd,sizeof(NewFile_cmd),100);
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)FileName,strlen(FileName),100);
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)&CR,1,100);

	/*Debemos esperar a que el modulo nos responda 12> para saber que esta listo
	* el 1 significa que la comunicacion serial esta lista el 2 significa que
	* la memoria SD esta lsita y el > significa que el modulo esta en command
	* mode
	*/
	HAL_UART_Receive(&OPENLOG_UART_HANDLE,(uint8_t*)buffer,sizeof(buffer),100);

	if(0 != memcmp(buffer,command_mode,sizeof(buffer))) {
		return OpenLog_ERROR;
	} else {
		Status = OpenLog_OK;
	}

	/*
	 * Se envia el comando "append " para decirle al modulo que todo lo que
	 * entre por el puerto serial se debe guardar en el archivo "FileName.txt"
	 * y el retorno de carro para finalizar el comando
	 */
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)Append_cmd,sizeof(Append_cmd),100);
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)FileName,sizeof(FileName),100);
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE,(uint8_t*)&CR,1,100);

	/*Debemos esperar a que el modulo nos responda 12< para saber que esta listo
	* el 1 significa que la comunicacion serial esta lista el 2 significa que
	* la memoria SD esta lsita y el < significa que el modulo esta listo
	* para recibir caracteres por el puerto serial
	*/
	HAL_UART_Receive(&OPENLOG_UART_HANDLE,(uint8_t*)buffer,sizeof(buffer),100);

	if(0 != memcmp(buffer,init_cmd,sizeof(buffer))) {
		return OpenLog_ERROR;
	} else {
		Status = OpenLog_OK;
	}

	// OpenLog esta listo para loggear todo lo que sea recibido por el puerto serial
	return Status;
}

void OpenLog_log(const char *fmt, ...)
{
	char buffer[300] = "";
	va_list args;
	va_start(args, fmt);
	vsnprintf(buffer, sizeof(buffer), fmt, args);
	va_end(args);

	HAL_UART_Transmit(&OPENLOG_UART_HANDLE, (uint8_t *) buffer, strlen(buffer), 250);
	HAL_UART_Transmit(&OPENLOG_UART_HANDLE, (uint8_t *) "\r\n", strlen("\r\n"), 250);
}



