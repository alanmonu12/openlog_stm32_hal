# OpenLog_STM32_HAL

Código de prueba
```
  /*
   * Se inicia el modulo OpenLog
   */
  OpenLog_init();
  /*
   * Se pone en command mode para poder crear archivos
   */
  OpenLog_CommandMode();
  /*
   * La funcion crea un archivo llamado log.txt y despues todos los datos recibidos
   * por el puerto serial donde esta conectado el OpenLog son guardados en ese
   * archivo
   */
  OpenLog_NewFile("Log_1.txt");
  
  HAL_UART_Transmit(&huart1,"Prueba OpenLog",14,100);
  ```