/*
 * OpenLog.h
 *
 *  Created on: 21/10/2018
 *      Author: Pavilion DM1
 */

#ifndef OPENLOG_H_
#define OPENLOG_H_

#include <stdarg.h>

#define CTRLZ 26

typedef enum
{
	OpenLog_OK = 0,
	OpenLog_ERROR
}OpenLog_status;


/*
 * @brief Funcion para inicializar el OpenLog
 */
OpenLog_status OpenLog_init(void);

/*
 * @brief Funcion para entrar en el modo comando del OpenLog
 */
OpenLog_status OpenLog_CommandMode(void);

/*
 * @brief Funcion para crear un nuevo documento en la memoria SD con el
 * nombre que le pasamos al funcion
 */
OpenLog_status OpenLog_NewFile(char* FileName);

void OpenLog_log(const char *fmt, ...);

#endif /* OPENLOG_H_ */
